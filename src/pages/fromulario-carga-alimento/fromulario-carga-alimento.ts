import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MemoryProvider } from './../../providers/memory/memory';
import { GeneralProvider } from './../../providers/general/general';
import { CommunicationsProvider } from './../../providers/communications/communications';
import {EntregaAlimentoProvider} from '../../providers/entrega-alimento/entrega-alimento';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import {DireccionPage} from '../direccion/direccion';
import 'rxjs/add/operator/timeout';
@IonicPage()
@Component({
  selector: 'page-fromulario-carga-alimento',
  templateUrl: 'fromulario-carga-alimento.html',
})
export class FromularioCargaAlimentoPage {

  constructor(public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams, public memory: MemoryProvider, public coms: CommunicationsProvider, public general: GeneralProvider,public alimento :EntregaAlimentoProvider,public alertCtrl: AlertController) {
  }

  public fecha = {
    fechaInicio: null,
    fechaInicioCrianza: null,
    horaInicio: null
  }

  //variabnles
  //variables generales

  public NumeroGuia;
  public SectorGuia;
  public PabellonesGuia;

 
  public ListaSectores:any=[];
  public ListaDietaGuia;
  public ListaPabellones:any=[];
  public ListaPabellonesSelecionados:any=[];

  public id_silo_1;
  public id_silo_2;
  //variables de la descarga

  public Pabellon;
  public KilosSilo1;
  public DietaSilo1;
  public KilosSilo2;
  public DietaSilo2;


  public iniciado=false;

 

  public id_sector;

  ionViewDidLoad(){
    
    console.log("user profile en load", this.memory.userProfile);
    
    if( this.memory.userProfile["permisos"]=="cerdo"){
        this.id_sector=2;
    }else /*if( this.memory.userProfile["permisos"]=="ave")*/{
        this.id_sector=1;
        
    }
    this.presentLoadingDefault();
    this.obtenerHora();
    this.general._menu.enable(true,'main-menu');
    this.general.controller = this;
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      // spinner: 'hide',
      content: 'Cargando información'
    });
  
    loading.present();

    var id ={"id":this.id_sector};
    //console.log(id);
    this.alimento.GetSectores(id).subscribe((data) => { // Success

      if(Object.keys(data).length>0){
              this.SectorGuia=(data[0]["nombre_corto"]);
              var id_sector={"id_sector":this.id_sector};
              this.alimento.GetPabellones(id_sector).subscribe((data) => { // Success
                if(Object.keys(data).length>0){
                  this.ListaPabellones=(data);
                
                loading.dismiss();
                }else{
                  this.showAlert("Error","no se encontraron pabellones");
                  this.navCtrl.setRoot(DireccionPage);
                }
                
             }
            );
      }else{
        this.showAlert("Error","no se encontro sector");
        this.navCtrl.setRoot(DireccionPage);
      }
     
    }
  );

  }
  ionViewWillEnter(){
    this.coms.getObject(this.memory.userToken).then(data => {
      //console.log(data);
      
    });
   
    this.coms.getInfo(this.memory.userToken).then(data => {
      
     // console.log(data);
    });
  
  }

  getDietas(){
      var tipoDieta={"tipo":this.memory.userProfile["permisos"]};
      this.alimento.GetDietas(tipoDieta).timeout(5000).subscribe((data) => { // Success
        if(Object.keys(data).length>0){
           this.ListaDietaGuia=(data);
        }else{
          this.showAlert("Error","No se encontraron  dietas");
          this.navCtrl.setRoot(DireccionPage);
        }
     
       //console.log(this.ListaDietaGuia);
    },()=>{
      this.showAlert("Error","No se logro traer dietas");
      this.navCtrl.setRoot(DireccionPage);
    }
  );
  }

  getPabellon(){
    var id_sector={"id_sector":this.id_sector};
    //console.log(id_sector)
    
    this.alimento.GetPabellones(id_sector).timeout(5000).subscribe((data) => { // Success
      if(Object.keys(data).length>0){
         this.ListaPabellones=(data);
      }else{
        this.showAlert("Error","No se encontraron pabellones");
        this.navCtrl.setRoot(DireccionPage);
      }
     
    },()=>{
      this.showAlert("Error","No se logro traer pabellones");
      this.navCtrl.setRoot(DireccionPage);
    }
  );
  }

  getSectores(){
    var id ={"id":this.id_sector};
    //console.log(id);
    this.alimento.GetSectores(id).timeout(5000).subscribe((data) => { // Success
      if(Object.keys(data).length>0){
        console.log(data);
        this.SectorGuia=(data[0]["nombre_corto"]);
      }else{
        this.showAlert("Error","no se encontraron  sectores");
        this.navCtrl.setRoot(DireccionPage);
      }
    
    },()=>{
      this.showAlert("Error","no se logro traer sectores");
      this.navCtrl.setRoot(DireccionPage);
    }
  );
  }

  getIdSilos(){
   
    var id ={"id_pabellon":this.Pabellon};
    this.alimento.GetIdSilo(id).timeout(5000).subscribe((data) => { // Success
      if(Object.keys(data).length>0){
        this.id_silo_1=data[0]["id"];
        this.id_silo_2=data[1]["id"];
      }else{
        this.showAlert("Error"," no se encontraron silos");
        this.navCtrl.setRoot(DireccionPage);
      }
      
      //console.log(this.id_silo_1+" "+this.id_silo_2);
    },()=>{
      this.showAlert("Error"," no se logro traer silos");
      this.navCtrl.setRoot(DireccionPage);
    }
  );
  }

 
  obtenerHora(){
    let fecha = new Date(Date.now());
    let hora = new Date(Date.now());
    let month = (fecha.getMonth()+1)+'';
    let minutos = (hora.getMinutes()+'');
 
    month = month.length == 1 ? '0'+month : month;
 
    let day = fecha.getDate() + '';
    day =  day.length == 1 ? '0'+day : day;
    minutos = minutos.length == 1 ? '0'+minutos : minutos;
 
    this.fecha.fechaInicio = fecha.getFullYear() + '-' + month + '-' + day;
    //this.fecha.fechaInicioCrianza = fecha.getFullYear() + '-' + month + '-' + day;
    this.fecha.horaInicio = hora.getHours() + ':' + minutos;
  }




  validarNumeroGuia(){
    if(this.NumeroGuia.trim().length==0){
      this.SectorGuia=null;
      this.showAlert("error","Completa el numero guía");
    }else{
      let loading = this.loadingCtrl.create({
        // spinner: 'hide',
        content: 'Cargando informacion'
      });
    
      loading.present();
  
      var guia ={"nro_guia":this.NumeroGuia};
      this.alimento.GetGuias(guia).timeout(5000).subscribe((data) => { // Success
        console.log(Object.keys(data).length);
        if(Object.keys(data).length>0){
          console.log(data[0]["dieta_id"]);
           this.DietaSilo1=data[0]["dieta_id"];
            this.DietaSilo2=data[0]["dieta_id"];
            this.ListaDietaGuia=data[0]["dieta"].toString();
            loading.dismiss();
        }else{
          loading.dismiss();
          this.showAlert("Error","El numero de guía ingresado no se encuentra registrado");
          this.DietaSilo1="";
            this.DietaSilo2="";
            this.ListaDietaGuia="";
        }
        
      },()=>{
        this.showAlert("Error","No se puede validar la guia en este momento");
        this.navCtrl.setRoot(DireccionPage);
      }

    );
  
    }//
  
  
     

     
  }


  Agregar(){

    var guia ={"nro_guia":this.NumeroGuia};
    

    this.alimento.GetGuias(guia).timeout(50000).subscribe((data) => { // Success
      console.log(Object.keys(data).length);
     
      if(Object.keys(data).length==0){
       this.showAlert("Error","El numero de  guía de despacho no existe, primero debe se agregada");
      }else{
        var objeto ={"NumeroGuia":this.NumeroGuia,"SectorGuia":this.id_sector,"DietaSilo":this.DietaSilo1,"Pabellon":this.Pabellon,"kilos_silo":this.KilosSilo1,"silo":this.id_silo_1,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio};
        var objeto2 ={"NumeroGuia":this.NumeroGuia,"SectorGuia":this.id_sector,"DietaSilo":this.DietaSilo2,"Pabellon":this.Pabellon,"kilos_silo":this.KilosSilo2,"silo":this.id_silo_2,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio};
    
  
      console.log(objeto);
      console.log(objeto2);
      var error=false;
  
        if(this.KilosSilo1 && this.DietaSilo1!=""){
          
           this.alimento.AgregarDescargaAlimento(objeto).timeout(5000).subscribe((data) => { // Success
                 console.log(data);
                },()=>{
                  this.showAlert("Error","No se logro agregar carga de elimento ,silo 1 ,intenar nuevamente");
                  error=true;
                }
             );
        }

        if(this.KilosSilo2 && this.DietaSilo2!=""){
          console.log("se envia silo 2 " + this.KilosSilo2)
          this.alimento.AgregarDescargaAlimento(objeto2).timeout(5000).subscribe((data) => { // Success
                console.log(data);
               },()=>{
                this.showAlert("Error","No se logro agregar carga de elimento ,silo 2 , intentar nuevamente");
                error=true;
              }
            );
       }
     
       console.log(error);
       if(error==false){
            if(this.KilosSilo1>0 || this.KilosSilo2>0){
              this.showAlert("Exito","se agrego la dieta");
              this.NumeroGuia=null;
              this.KilosSilo1=null;
              this.KilosSilo2=null;
              this.DietaSilo1=null;
              this.DietaSilo2=null;
              this.Pabellon=null;
              this.obtenerHora();
            }else{
              this.showAlert("Error","debe ingresar kilos en almenos un silo");
            }
          
       }
  

      }
    },()=>{
      this.showAlert("Error","No fue posible ejecutar la accion")
    }
  );



     
    
  }


  showAlert(titulo,mensaje) {
    const alert = this.alertCtrl.create({
      title: titulo,
      subTitle: mensaje,
      buttons: ['OK']
    });
    alert.present();
  }





}
