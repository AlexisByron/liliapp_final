import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MemoryProvider } from './../../providers/memory/memory';
import { GeneralProvider } from './../../providers/general/general';
import {EntregaAlimentoProvider} from '../../providers/entrega-alimento/entrega-alimento';
import { AlertController } from 'ionic-angular';
import {MortalidadProvider} from '../../providers/mortalidad/mortalidad';
import { LoadingController } from 'ionic-angular';
import {DireccionPage} from '../direccion/direccion';
import 'rxjs/add/operator/timeout';
/**
 * Generated class for the RegistroGuiaDespachoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registro-guia-despacho',
  templateUrl: 'registro-guia-despacho.html',
})
export class RegistroGuiaDespachoPage {

  constructor(public loadingCtrl: LoadingController, public memory: MemoryProvider,public navCtrl: NavController, public navParams: NavParams, public general: GeneralProvider,public alimento:EntregaAlimentoProvider,public alertCtrl: AlertController,public mortalidad:MortalidadProvider) {
  }

  public id_sector;
  public NumeroGuia;
  public SectorGuia;

  public fecha = {
    fechaInicio: null,
    fechaInicioCrianza: null,
    horaInicio: null
  }

  public ListaDietaGuia:any=[];
  public DietaGuia;
  public KilosGuia;
  public id_crianza;

  getCrianzaCerdo(){
    this.mortalidad.getUltimacrianzaCerdo().subscribe((data) => { // Success
      if(Object.keys(data).length>0){
        console.log("tiene datos");
        this.id_crianza=data[0]["id_crianza"];
      }else{
        console.log("no tiene datos");
        this.showAlert("Error","No existen crianzas de cerdos registradas");
        this.navCtrl.setRoot(DireccionPage);
      }
    }
  );
}

getCrianzaAves(){
    this.mortalidad.getUltimacrianzaAves().subscribe((data) => { // Success
        if(Object.keys(data).length>0){
          console.log("tiene datos");
          this.id_crianza=data[0]["id_crianza"];
        }else{
          console.log("no tiene datos");
          this.showAlert("Error","No existen crianzas de aves registradas");
          this.navCtrl.setRoot(DireccionPage);
        }
      
    }
  );
}

  obtenerHora(){
    let fecha = new Date(Date.now());
    let hora = new Date(Date.now());
    let month = (fecha.getMonth()+1)+'';
    let minutos = (hora.getMinutes()+'');
 
    month = month.length == 1 ? '0'+month : month;
 
    let day = fecha.getDate() + '';
    day =  day.length == 1 ? '0'+day : day;
    minutos = minutos.length == 1 ? '0'+minutos : minutos;
 
    this.fecha.fechaInicio = fecha.getFullYear() + '-' + month + '-' + day;
    //this.fecha.fechaInicioCrianza = fecha.getFullYear() + '-' + month + '-' + day;
    this.fecha.horaInicio = hora.getHours() + ':' + minutos;
  }

  ionViewDidLoad() {
    
    this.obtenerHora();
    if( this.memory.userProfile["permisos"]=="cerdo"){
      this.id_sector=2;
     this.getCrianzaCerdo();
  }else if( this.memory.userProfile["permisos"]=="ave"){
      this.id_sector=1;
      this.getCrianzaAves();
  }

  this.presentLoadingDefault();


  this.general._menu.enable(true,'main-menu');
  this.general.controller = this;
  }


  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      // spinner: 'hide',
      content: 'Cargando información'
    });
  
    loading.present();

    var id ={"id":this.id_sector};
    //console.log(id);
    this.alimento.GetSectores(id).subscribe((data) => { // Success
     // console.log(data);
      this.SectorGuia=(data[0]["nombre_corto"]);

      var codigoDieta={"tipo":this.memory.userProfile["permisos"]};
      this.alimento.GetDietas(codigoDieta).subscribe((data) => { // Success
      if(Object.keys(data).length>0){
      console.log(data);
      this.ListaDietaGuia=(data);
      loading.dismiss();
      }else{
        this.showAlert("Error","No existen dietas");
        this.navCtrl.setRoot(DireccionPage);
      }
      
      
       
    }
  );
    }
  );



  
    
  }


  getSectores(){
    var id ={"id":this.id_sector};
    //console.log(id);
    this.alimento.GetSectores(id).subscribe((data) => { // Success
      if(Object.keys(data).length>0){
         this.SectorGuia=(data[0]["nombre_corto"]);
      }else{
        this.showAlert("Error","No existe sector");
        this.navCtrl.setRoot(DireccionPage);
      }
     
    }
  );
  }

  
  getDietas(){
    var codigoDieta={"tipo":this.memory.userProfile["permisos"]};
    this.alimento.GetDietas(codigoDieta).subscribe((data) => { // Success
      
      if(Object.keys(data).length>0){
        console.log(data);
    this.ListaDietaGuia=(data);
     //console.log(this.ListaDietaGuia);
      }else{
        this.showAlert("Error","No existen dietas");
        this.navCtrl.setRoot(DireccionPage);
      }
    
  }
);
}

Agregar(){
  var guia ={"nro_guia":this.NumeroGuia};
  console.log(this.id_crianza);

  this.alimento.GetGuias(guia).timeout(5000).subscribe((data) => { // Success
    
    if(Object.keys(data).length==0){
      var objeto={"sector":this.id_sector,"nro_guia":this.NumeroGuia,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio,"dieta":this.DietaGuia,"kilosGuia":this.KilosGuia,"crianza":this.id_crianza};
      //console.log(objeto);
    this.alimento.AgregarGuiaDespacho(objeto).subscribe((data) => { // Success
      this.showAlert("Exito","Guía de despacho agregada exitosamente");
      this.NumeroGuia=null;
      this.KilosGuia=null;
      this.DietaGuia=null;

    },() => {
      this.showAlert("Error","No se agrego la guía de despacho, intentar nuevamente");
      
    }
  );
    }else{
      this.showAlert("Error","Guía de despacho ya está agregada ");
    }
  });



  //console.log(objeto);
}

showAlert(titulo,mensaje) {
  const alert = this.alertCtrl.create({
    title: titulo,
    subTitle: mensaje,
    buttons: ['OK']
  });
  alert.present();
}

}
