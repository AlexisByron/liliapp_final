import { MemoryProvider } from './../../providers/memory/memory';
import { GeneralProvider } from './../../providers/general/general';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DireccionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-direccion',
  templateUrl: 'direccion.html',
})
export class DireccionPage {

  public formulario = null;
  public permiso  = null;
  public sector = null;
  public page="RegistroGuiaDespachoPage";

  constructor(public navCtrl: NavController, public navParams: NavParams, public general: GeneralProvider, public memory: MemoryProvider) {
  }

  ionViewDidLoad() {
    this.general._menu.enable(true,'main-menu');
    this.general.controller = this;
    console.log(this.memory.userProfile);
    this.permiso = this.memory.userProfile.permisos;
    this.sector = this.memory.userProfile.sector;
    this.general.mostrarSelectorFormulario = false;
  }

  ionViewDidLeave(){
    this.general.mostrarSelectorFormulario = true;
  }

  onToFormulario(){
    //console.log(page);
    this.navCtrl.setRoot(this.page);
  }

 
  
}
