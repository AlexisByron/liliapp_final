import {GeneralProvider} from '../../providers/general/general';
import {CommunicationsProvider} from '../../providers/communications/communications';
import {Component} from '@angular/core';
import {NavController, IonicPage} from 'ionic-angular';
import {MemoryProvider} from '../../providers/memory/memory';
import {ToastController} from 'ionic-angular/components/toast/toast-controller';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { StatusBar } from '@ionic-native/status-bar';


@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

export class HomePage {

    public sessionData = {
        //username: 'mortalidad',
        //password: 'agrosuper4.0'
        username: '',
        password: ''
    };

    public verificando = true;

    constructor(private statusBar: StatusBar, public navCtrl: NavController, private coms: CommunicationsProvider, public memory: MemoryProvider, public general: GeneralProvider, private toastCtrl: ToastController ,public pantalla:ScreenOrientation) {
        //this.statusBar.backgroundColorByHexString('#060606');
    }

    login() {
        if (this.verificando){
            return;
        }

        console.log('Iniciando sesion');
        this.general.presentLoader('Iniciando sesión');

        this.coms.login(this.sessionData.username, this.sessionData.password).then(data => {
            this.general.dismissLoader();
            console.log('respuesta de inicio de sesion', data);
            console.log('recuperacion de datos de usuario: ', data['userData']);
            if( data['userData']['permisos'] == 'contador' ) {
                this.general.mostrarSelectorFormulario = false;
            }
            this.memory.user = data['userData'];
            this.memory.token = data['taut'];

            this.memory.sector = {
                capacidadPabellones: [900, 900, 900, 900, 900, 900, 900, 900, 900, 900],
                crianzaActual: "183",
                fechaCrianza: "2018-09-28T00:00:00.008Z",
                nombre: data["userData"]["sector"],
                usoPabellones: [900, 900, 900, 900, 900, 900, 900, 900, 900, 900],
            };

            //console.log(data);
            if (data['userData'].permisos === "contador") {
                this.navCtrl.setRoot('ContadorPage').then();
            } else if (data['userData'].permisos === "crianza") {
                this.navCtrl.setRoot('FormularioCrianzaPage').then();
            } else if (data['userData'].username === "gabriel") {
                this.navCtrl.setRoot('FormularioCrianzaPage').then();
            } else if (data['userData'].permisos === "dieta") {
                this.navCtrl.setRoot('DietaPage').then();
            } else if (data['userData'].permisos === "ave") {
                this.navCtrl.setRoot('DireccionPage').then();
            } else if (data['userData'].permisos === "cerdo") {
                this.navCtrl.setRoot('DireccionPage').then();
            }
            else {
                this.goToMain('MainviewPage');
            }


        }).catch(err => {
            this.general.dismissLoader();
            this.presentError('Error de conexión');
            if (err === 403) {
                this.general.alerta_generica('¡Error!', "La combinacion de nombre de usuario y contraseña suministrados no son válidos");
            }
        })
    }

    ionViewDidLoad() {
        //this.pantalla.lock(this.pantalla.ORIENTATIONS.PORTRAIT);
        //verificar sesion previa
        console.log('verificar sesion previa');
        this.verificando = true;
        this.general._menu.enable(false, 'main-menu');
        this.memory.verificarSesionPrevia().then(() => {
            console.log(this.memory.userProfile);
            //hay token, validar con servidor
            console.log('hay sesion previa');
            this.coms.testToken(this.memory.userToken).then(() => {
                this.verificando = false;
                if (this.memory.userProfile != null) {
                    this.coms.perfilUsuario(this.memory.userProfile['username'], this.memory.userToken).then(res => {
                        console.log("aqui el res user", res);
                        console.log( this.memory.userProfile);
                        
                        this.memory.user = res['userData']
                    });

                    console.log("hola memory user", this.memory.userProfile);
                    if (this.memory.userProfile.permisos === "crianza") {
                        this.verificando = false;
                        this.goToMain('FormularioCrianzaPage');
                    } else if (this.memory.userProfile.permisos === "contador") {
                        this.verificando = false;
                        this.goToMain('ContadorPage');
                    } else if (this.memory.userProfile.permisos === "dieta") {
                        this.verificando = false;
                        console.log("aqui los permisos dieteticos", this.memory.userProfile);
                        this.goToMain('FromularioCargaAlimentoPage');
                    } else if (this.memory.userProfile.permisos === "cerdo") {
                        this.verificando = false;
                        this.goToMain('DireccionPage');
                    } else if (this.memory.userProfile.permisos === "ave") {
                        this.verificando = false;
                        this.goToMain('DireccionPage');
                    }
                    else {
                        this.verificando = false;
                        this.goToMain('MainviewPage');
                    }
                } else {
                    //hay token pero no informacion de usuario, eliminar token y forzar nuevo logeo
                    this.verificando = false;
                    this.memory.token = null;
                }
            }).catch(err => {
                //anular token? puede ser error de conexion
                console.log('problema validando token: ', err);
                this.verificando = false;
            })
        }).catch(() => {
            console.log('no hay sesion previa');
            this.verificando = false;
        })
    }

    goToMain(page) {
        // solicitar informacion de sector y crianza antes de seguir;
        // luego solicitar control actual en memoria y contrastar con servidor
        console.log('solicitando info de sector');
        this.general.presentLoader('Sincronizando informacion de sector ' + this.memory.userProfile['sector']);
        this.coms.sectorInfo(this.memory.userProfile['sector'], this.memory.userToken).then(data => {
            this.general.dismissLoader();
            console.log('se recibio info de sector', data);
            this.navCtrl.setRoot(page).then();
            this.memory.sector = data['sector'];
        }).catch(err => {
            this.general.dismissLoader();
            console.log('error con info de sector: ', err);
        })
    }

    presentError(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top'
        });
        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });
        toast.present().then();
    }

}
