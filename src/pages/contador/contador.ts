import {MemoryProvider} from '../../providers/memory/memory';

import {GeneralProvider} from '../../providers/general/general';
import {CommunicationsProvider} from '../../providers/communications/communications';
import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, AlertController, ToastController} from 'ionic-angular';
import {EntregaAlimentoProvider} from '../../providers/entrega-alimento/entrega-alimento';
import {MortalidadProvider} from '../../providers/mortalidad/mortalidad'; 
import { LoadingController } from 'ionic-angular';
import {DireccionPage} from '../direccion/direccion';
import 'rxjs/add/operator/timeout';

@IonicPage()
@Component({
    selector: 'page-contador',
    templateUrl: 'contador.html',
})
export class ContadorPage {




    constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public coms: CommunicationsProvider, public general: GeneralProvider, public memory: MemoryProvider, private toastCtrl: ToastController,public alimento:EntregaAlimentoProvider,private mortalidad :MortalidadProvider) {}

    public fecha = {
        fechaInicio: null,
        horaInicio: null
      }
    public total;
    public pabellones;
    public pabellon;
   
    public Causales=[];
    public crianza;
    public Mortalidad=[];
    public SectorGuia;
    public id_sector;

    ionViewDidLoad() {
      
        this.general.controller = this;
        this.general._menu.enable(true, 'main-menu');
    }

    ionViewDidEnter() {
        var id
        this.obtenerHora();
        console.log(this.crianza)
       
        if( this.memory.userProfile["permisos"]=="cerdo"){
            id=2;
            //this.Mortalidad[0].id_sector=2;

        }else if(this.memory.userProfile["permisos"]=="ave"){
            id=1;
            //this.Mortalidad[0].id_sector=1;
            
          
        }
        
        this.id_sector=id;
        this.setMortalidad();
        this.presentLoadingDefault(id);
       
    }

    presentLoadingDefault(idSector) {
      let loading = this.loadingCtrl.create({
        // spinner: 'hide',
        content: 'Cargando información'
      });
    
      loading.present();

      var id_sector={"id_sector":idSector};
      this.alimento.GetPabellones(id_sector).timeout(5000).subscribe((data) => { // Success
        if(Object.keys(data).length>0){
          console.log(data);
          this.pabellones=data;
          
          var id1={"id":idSector};
          this.alimento.GetSectores(id1).timeout(5000).subscribe((data) => { // Success
            
              if(Object.keys(data).length>0){
                console.log(data);
            this.SectorGuia=(data[0]["nombre_corto"]);
    
            var idMortalidades ={"id":idSector}
    
            if(idSector==1){
              this.mortalidad.getUltimacrianzaAves().timeout(5000).subscribe((data) => { // Success
                if(Object.keys(data).length>0){

                  this.crianza=data[0]["id_crianza"];
                  this.Mortalidad[0].nro_crianza=this.crianza;
                  console.log(data);
      
                  this.mortalidad.getMortalidadesAsistidas(idMortalidades).timeout(5000).subscribe((data) => { // Success
                    if(Object.keys(data).length>0){

                      console.log(data);
                      for (let index = 0; index <  Object.keys(data).length; index++) {
                         var causa={"causa":data[index]["mortalidad"],"id_causa":data[index]["id"],"valor":0,"nro_crianza":this.crianza,"pabellon":this.pabellon,"id_sector":this.id_sector,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio,"user":this.memory.userProfile["username"],"comentario":""};
                         this.Causales.push(causa);
                      }
                        loading.dismiss();
                    }else{
                      this.showAlert("Error","No se encontraron mortalidades de aves");
                    this.navCtrl.setRoot(DireccionPage);
                    }
                   },()=>{
                    this.showAlert("Error","No se encontro crianza de aves");
                    this.navCtrl.setRoot(DireccionPage);
                  }
                 ); 
          
                }else{
                  this.showAlert("Error","No se logro traer numero de crianza");
                  this.navCtrl.setRoot(DireccionPage);
                }
                
            },()=>{
              this.showAlert("Error","No se logro traer numero de crianza");
              this.navCtrl.setRoot(DireccionPage);
            }
          );
            }else if(idSector==2){
              this.mortalidad.getUltimacrianzaCerdo().timeout(5000).subscribe((data) => { // Success
                  if(Object.keys(data).length>0){
                    this.crianza=data[0]["id_crianza"];
                this.mortalidad.getMortalidadesAsistidas(idMortalidades).timeout(5000).subscribe((data) => { // Success
                  if(Object.keys(data).length>0){
                    console.log(data);
                    for (let index = 0; index <  Object.keys(data).length; index++) {
                       var causa={"causa":data[index]["mortalidad"],"id_causa":data[index]["id"],"valor":0,"nro_crianza":this.crianza,"pabellon":this.pabellon,"id_sector":this.id_sector,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio,"user":this.memory.userProfile["username"],"comentario":""};
                       this.Causales.push(causa);
                    }
                    loading.dismiss();
                  }else{
                    this.showAlert("Error","No se encontraron mortalidades de cerdo");
                  this.navCtrl.setRoot(DireccionPage);
                  }
                 },()=>{
                  this.showAlert("Error","No se logro traer mortalidades");
                  this.navCtrl.setRoot(DireccionPage);
                }
               ); 
        
              
                 console.log(data);
                  }else{
                    this.showAlert("Error","No se encontro numero de crianza de cerdo");
                    this.navCtrl.setRoot(DireccionPage);
                  }
                },()=>{
                  this.showAlert("Error","No se logro traer numero de crianza de cerdo");
                  this.navCtrl.setRoot(DireccionPage);
                }
              );
            }
              }else{
                this.showAlert("Error","No se encontraron sectores")
            this.navCtrl.setRoot(DireccionPage);
              }
          },()=>{
            this.showAlert("Error","No se logro traer sectores")
            this.navCtrl.setRoot(DireccionPage);
          }
        );
        }else{
          this.showAlert("Error","No se encontraron pabellones");
          this.navCtrl.setRoot(DireccionPage);
        }
      },()=>{
        this.showAlert("Error","No se logro traer pabellones")
        this.navCtrl.setRoot(DireccionPage);
      }
    );
  
     
 
      
    }
  

    getSectores(id){
      var id1 ={"id":id};
      //console.log(id);
      this.alimento.GetSectores(id1).subscribe((data) => { // Success
        if(Object.keys(data).length>0){
          console.log(data);
        this.SectorGuia=(data[0]["nombre_corto"]);
        }else{
          this.showAlert("Error","no se encontro sector");
          this.navCtrl.setRoot(DireccionPage);
        }
        
      }
    );
    }

    getCrianzaCerdo(){
        this.mortalidad.getUltimacrianzaCerdo().subscribe((data) => { // Success
        this.crianza=data[0]["id_crianza"];
           
         console.log(data);
        }
      );
    }

    setMortalidad(){
        var obj={"causa":"Contador General","id_causa":9,"valor":0,"nro_crianza":this.crianza,"pabellon":this.pabellon,"id_sector":this.id_sector,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio,"user":this.memory.userProfile["username"],"comentario":""};
        this.Mortalidad.push(obj);
    }

    getCrianzaAves(){
        this.mortalidad.getUltimacrianzaAves().subscribe((data) => { // Success
            this.crianza=data[0]["id_crianza"];
            console.log(data);
        }
      );
    }

    poblarCausales(){
       
        var id;
        if( this.memory.userProfile["permisos"]=="cerdo"){
         id=2;
      }else if( this.memory.userProfile["permisos"]=="ave"){
          id=1;
      }
    
        var idMortalidades ={"id":id}
         this.mortalidad.getMortalidadesAsistidas(idMortalidades).subscribe((data) => { // Success
           // console.log(data);
         for (let index = 0; index <  Object.keys(data).length; index++) {
            
            var causa={"causa":data[index]["mortalidad"],"id_causa":data[index]["id"],"valor":0,"nro_crianza":this.crianza,"pabellon":this.pabellon,"id_sector":this.id_sector,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio,"user":this.memory.userProfile["username"],"comentario":""};
            this.Causales.push(causa);
         }
          }
        ); 
    
     
       console.log(this.Causales);
      }

      quitarValorCausa(valor){
        if(this.Causales[valor].valor>0){
        this.Causales[valor].valor--;
        }
        this.obtenerTotal();
      }

      agregarValorCausa(valor){
   
        this.Causales[valor].valor++;
        this.obtenerTotal();

        
        if(this.Causales[valor].comentario=="" ){

            const prompt = this.alertCtrl.create();
            this.Causales[valor].comentario=null;
            prompt.setTitle('Desea ingresar un comentario para causa : '+this.Causales[valor].causa);
           
             
            prompt.addInput({
             type: 'textarea',
             name: 'Comentario',
             placeholder: 'Ingresar comentario'
           });
       
           prompt.addButton({
             text: 'No',
             handler: data => {
                this.Causales[valor].comentario=null;
              
             }
           });
       
           prompt.addButton({
             text: 'Registrar',
             handler: data => {
                this.Causales[valor].comentario=data.Comentario;
               console.log(data);
             }
           });
       
          
       
             prompt.present();
           }
    
    }

    obtenerTotal(){
        this.total=0;
        this.Causales.forEach(element => {
        this.total=this.total+Number(element.valor);
        });
        this.total=this.total+Number(this.Mortalidad[0].valor);
      }


    CambioPabellon(){
       // alert(this.pabellon);
        this.Causales.forEach(element => {
            element.pabellon=this.pabellon;
        });

        this.Mortalidad[0].pabellon=this.pabellon;
        console.log(this.Mortalidad);
      }
    


    mortalidadAdd(){
        this.Mortalidad[0].valor++;

        if(this.Mortalidad[0].comentario=="" ){

            const prompt = this.alertCtrl.create();
            this.Mortalidad[0].comentario=null;
            prompt.setTitle('Desea ingresar un comentario para causa N.A: ');
           
             
            prompt.addInput({
             type: 'textarea',
             name: 'Comentario',
             placeholder: 'Ingresar comentario'
           });
       
           prompt.addButton({
             text: 'No',
             handler: data => {
               this.Mortalidad[0].comentario=null;
              
             }
           });
       
           prompt.addButton({
             text: 'Registrar',
             handler: data => {
                this.Mortalidad[0].comentario=data.Comentario;
               console.log(data);
             }
           });
       
          
       
             prompt.present();
           }
    }

    mortalidadMenos(){
        if(this.Mortalidad[0].valor>0){
            this.Mortalidad[0].valor--;
        }
    }

    getPabellon(idSector){
        var id_sector={"id_sector":idSector};
        this.alimento.GetPabellones(id_sector).subscribe((data) => { // Success
        console.log(data);
        this.pabellones=data;
        }
      );
    }
    
    obtenerHora(){
        let fecha = new Date(Date.now());
        let hora = new Date(Date.now());
        let month = (fecha.getMonth()+1)+'';
        let minutos = (hora.getMinutes()+'');
     
        month = month.length == 1 ? '0'+month : month;
     
        let day = fecha.getDate() + '';
        day =  day.length == 1 ? '0'+day : day;
        minutos = minutos.length == 1 ? '0'+minutos : minutos;
     
        this.fecha.fechaInicio = fecha.getFullYear() + '-' + month + '-' + day;
        //this.fecha.fechaInicioCrianza = fecha.getFullYear() + '-' + month + '-' + day;
        var a ;
        if(hora.getHours().toString().length==1){
           a = "0"+hora.getHours();
        }else{
          a = hora.getHours();
        }
        this.fecha.horaInicio = a+ ':' + minutos;


      console.log(this.fecha);

    }

    Eviar(){

        var id
      
        if( this.memory.userProfile["permisos"]=="cerdo"){
            id=2;
           
        }else if(this.memory.userProfile["permisos"]=="ave"){
            id=1;
            
        }

        var error="";
       // console.log( this.Causales);
        //console.log(this.Mortalidad)
       
        this.Causales.forEach(element => {
            if(element.valor>0){
                console.log(element)
                if(id==1){
                  console.log("aves");
                  console.log(element);
                  this.mortalidad.InsertMortalidadAves(element).timeout(7000).subscribe((data) => { // Success
                  console.log(data);
                  //this.showAlert("Exito","Registro agregado");
                },()=>{
                  error="No se logro agrear mortalidad aves";
                }
              );
                }else if(id==2){
                  
                  this.mortalidad.InsertMortalidadCerdos(element).timeout(7000).subscribe((data) => { // Success
                  console.log(data);
                  //this.showAlert("Exito","Registro agregado");
                  },()=>{
                    error="No se logro agrear mortalidad cerdos";
                  }
                );
                }
           
              }
          });

          if(this.Mortalidad[0].valor>0){ // inserta contador general
            this.Mortalidad[0].nro_crianza=this.crianza;
            if(id==1){
                console.log("aca tambien")
                this.mortalidad.InsertMortalidadAves(this.Mortalidad[0]).timeout(7000).subscribe((data) => { // Success
                console.log(data);
               // this.showAlert("Exito","Registro agregado");
              },()=>{
                error="No se logro agrear mortalidad aves";
              }
            );
              }else if(id==2){
               
                this.mortalidad.InsertMortalidadCerdos(this.Mortalidad[0]).timeout(7000).subscribe((data) => { // Success
                console.log(data);
                //this.showAlert("Exito","Registro agregado");
                },()=>{
                  error="No se logro agrear mortalidad cerdos";
                }
              );
              }
          }
         
          console.log(error)
          if(error==""){
            this.showAlert("Exito","Registro agregado");
            this.Mortalidad=[];
          
          this.Causales=[];
          this.total=0;
          this.obtenerHora();
          this.poblarCausales();
          this.setMortalidad();
          }else{
            this.showAlert("Error",error);
          }
          
    }

    showAlert(titulo,mensaje) {
        const alert = this.alertCtrl.create({
          title: titulo,
          subTitle: mensaje,
          buttons: ['OK']
        });
        alert.present();
      }

}
