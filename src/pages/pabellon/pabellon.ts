import { CommunicationsProvider } from './../../providers/communications/communications';
import { GeneralProvider } from './../../providers/general/general';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MemoryProvider } from '../../providers/memory/memory';
import {MortalidadProvider} from '../../providers/mortalidad/mortalidad'; 
import { AlertController } from 'ionic-angular';
import {DireccionPage} from '../direccion/direccion';
import { LoadingController } from 'ionic-angular'
import 'rxjs/add/operator/timeout';


@IonicPage()
@Component({
  selector: 'page-pabellon',
  templateUrl: 'pabellon.html',
})
export class PabellonPage {


  public userProfile = null;

  constructor(public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams, 
    public memory : MemoryProvider, public general : GeneralProvider,
     private coms : CommunicationsProvider,private mortalidad :MortalidadProvider,public alertCtrl: AlertController) {
      // console.log(navParams.get("datos"));
       this.sector=navParams.get("datos")["sector"]["sector"];
       var id =navParams.get("datos")["sector"]["id"];
     
       this.id_sector={"id_sector":id.id};
       console.log(this.id_sector);
       this.pabellon=navParams.get("datos")["pabellon"];
       this.crianza=navParams.get("datos")["nro_crianza"];
       this.presentLoadingDefault();
     // this.obtenerHora();
  }

  public causales=[];
  public total=0;
  public sector="";
  public pabellon=0;
  public crianza=0;
  public id_sector;

  public fecha = {
    fechaInicio: null,
    horaInicio: null
  }



  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      // spinner: 'hide',
      content: 'Cargando información'
    });
  
    loading.present();

    this.obtenerHora();
    var id;
    if( this.memory.userProfile["permisos"]=="cerdo"){
     id=2;
  }else if( this.memory.userProfile["permisos"]=="ave"){
      id=1;
      
  }
    var idMortalidades ={"id":id}
    
     this.mortalidad.getMortalidades(idMortalidades).timeout(5000).subscribe((data) => { // Success
      if(Object.keys(data).length>0){
        for (let index = 0; index <  Object.keys(data).length; index++) {
        var causa={"causa":data[index]["mortalidad"],"id_causa":data[index]["id"],"valor":0,"nro_crianza":this.crianza,"pabellon":this.pabellon,"id_sector":this.id_sector.id_sector,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio,"user":this.memory.userProfile["username"],"comentario":""};
        this.causales.push(causa);
       
     }
     loading.dismiss();
      }else{
        this.showAlert("Error","No se encontraron mortalidades");
        this.navCtrl.setRoot(DireccionPage);
      }
     
     
      },() => {
        this.showAlert("Error","No se logro traer causas");
        
      }
    ); 
    //this.poblarCausales();
  
    
  }

  
  obtenerHora(){
    let fecha = new Date(Date.now());
    let hora = new Date(Date.now());
    let month = (fecha.getMonth()+1)+'';
    let minutos = (hora.getMinutes()+'');
 
    month = month.length == 1 ? '0'+month : month;
 
    let day = fecha.getDate() + '';
    day =  day.length == 1 ? '0'+day : day;
    minutos = minutos.length == 1 ? '0'+minutos : minutos;
 
    this.fecha.fechaInicio = fecha.getFullYear() + '-' + month + '-' + day;
    //this.fecha.fechaInicioCrianza = fecha.getFullYear() + '-' + month + '-' + day;
    this.fecha.horaInicio = hora.getHours() + ':' + minutos;
  }
  
  poblarCausales(){
    //console.log( this.memory.userProfile)
    var id;
    if( this.memory.userProfile["permisos"]=="cerdo"){
     id=2;
  }else if( this.memory.userProfile["permisos"]=="ave"){
      id=1;
      
  }
    var idMortalidades ={"id":id}
    
     this.mortalidad.getMortalidades(idMortalidades).timeout(5000).subscribe((data) => { // Success
     
     for (let index = 0; index <  Object.keys(data).length; index++) {
        var causa={"causa":data[index]["mortalidad"],"id_causa":data[index]["id"],"valor":0,"nro_crianza":this.crianza,"pabellon":this.pabellon,"id_sector":this.id_sector.id_sector,"fecha":this.fecha.fechaInicio+" "+this.fecha.horaInicio,"user":this.memory.userProfile["username"],"comentario":""};
        this.causales.push(causa);
       
     }
      },()=>{
        this.showAlert("Error","No se lograron traer mortalidades")
      }
    ); 

   
   // console.log(this.causales);
  }

  

  agregarValorCausa(valor){
   
    this.causales[valor].valor++;
    console.log();
    if(this.causales[valor].comentario=="" ){

     const prompt = this.alertCtrl.create();

     prompt.setTitle('Desea ingresar un comentario para causa: ');
     prompt.setSubTitle(this.causales[valor].causa);
      
     prompt.addInput({
      type: 'textarea',
      name: 'Comentario',
      placeholder: 'Ingresar comentario'
    });

    prompt.addButton({
      text: 'No',
      handler: data => {
        this.causales[valor].comentario=null;
       
      }
    });

    prompt.addButton({
      text: 'Registrar',
      handler: data => {
        this.causales[valor].comentario=data.Comentario;
        console.log(data);
      }
    });

   

      prompt.present();
    }
    
    this.obtenerTotal();
  }

  cambioValor(i){
    console.log(this.causales[i]);
  }

  quitarValorCausa(valor){
    
    if(this.causales[valor].valor>0){
    this.causales[valor].valor--;
    }
    this.obtenerTotal();
  }

  obtenerTotal(){
    
    this.total=0;
    this.causales.forEach(element => {
    
    this.total=this.total+Number(element.valor);
    });
   
  }

  guardar(){
    this.obtenerHora();
    console.log("estoy guardando");
    var a=0;
    var error="";
    //console.log(this.causales)
   this.causales.forEach(element => {
      
      if(element.valor>0){
        console.log(element)
        if(this.id_sector.id_sector==1){
          
          this.mortalidad.InsertMortalidadAves(element).timeout(8000).subscribe((data) => { // Success
          console.log(data);
        },()=>{
           error="no se agregaron mortalidades de aves";
         // this.showAlert("Error","no se agrego mortalidad");
        }
      );
        }
        else if(this.id_sector.id_sector==2){
          console.log("Esto tampoco");
          this.mortalidad.InsertMortalidadCerdos(element).timeout(8000).subscribe((data) => { // Success
          console.log(data);
          },()=>{
            error="no se agregaron mortalidades de cerdos";
          }
        );
        }
   
      }
     
  
       
    });

    if(error==""){
  this.showAlert("Exito","Registro agregado");
    this.causales=[];
   
    this.total=0;
    this.obtenerHora();
    this.poblarCausales();
    }else{
      this.showAlert("Error",error);
    }
  
  }

  showAlert(titulo,mensaje) {
    const alert = this.alertCtrl.create({
      title: titulo,
      subTitle: mensaje,
      buttons: ['OK']
    });
    alert.present();
  }
   
}
