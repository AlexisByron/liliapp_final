import {GeneralProvider} from '../../providers/general/general';
import {Component, ViewChild, ElementRef} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {MemoryProvider} from '../../providers/memory/memory';
import * as moment from 'moment';
import {CommunicationsProvider} from '../../providers/communications/communications';
//import { IBeacon } from '@ionic-native/ibeacon';
import {Platform, DateTime} from 'ionic-angular';
import {HomePage} from '../home/home';
import {EntregaAlimentoProvider} from '../../providers/entrega-alimento/entrega-alimento';
import {MortalidadProvider} from '../../providers/mortalidad/mortalidad';
import { LoadingController } from 'ionic-angular'
import {DireccionPage} from '../direccion/direccion';

@IonicPage()
@Component({
    selector: 'page-mainview',
    templateUrl: 'mainview.html',
})
export class MainviewPage {

    //@ViewChild('dateinput') dateInput

    @ViewChild('dateinput') timePicker: DateTime;
    constructor(public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams, public memory: MemoryProvider, private coms: CommunicationsProvider, public general: GeneralProvider/* , private ibeacon: IBeacon */, private alertCtrl: AlertController,public alimento:EntregaAlimentoProvider,public mortalidad:MortalidadProvider) {
        
    }
    public origen;
    public pabellones;
    public cantidadP;//cantidad pabellones segun sector
    public userProfile = null;
    public id_crianza;
    public fecha = {
        fechaInicio: null,
        horaInicio: null
      }

      public sectorGuia={};

    ionViewDidEnter() {
       // console.log('Main View');
        this.obtenerHora();
    }


    ionViewDidLoad() {
       var id
        if( this.memory.userProfile["permisos"]=="cerdo"){
            id=2;
            this.getCrianzaCerdo();
        }else if(this.memory.userProfile["permisos"]=="ave"){
            id=1;
            this.getCrianzaAves();
        }

        this.presentLoadingDefault(id);

       
        this.userProfile = this.memory.userProfile;
        this.general._menu.enable(true, 'main-menu');
        this.general.controller = this;
        
    }

    presentLoadingDefault(id) {
        let loading = this.loadingCtrl.create({
          // spinner: 'hide',
          content: 'Cargando información'
        });
      
        loading.present();
    
        var id1 ={"id":id};
        //console.log(id);
        this.alimento.GetSectores(id1).subscribe((data) => { // Success
          if(Object.keys(data).length>0){
            this.sectorGuia={"sector":data[0]["nombre_corto"],"id":id1};

            var id_sector={"id_sector":id};
            this.alimento.GetPabellones(id_sector).subscribe((data) => { // Success
              if(Object.keys(data).length>0){
                       this.pabellones=data;
                      this.cantidadP=(this.pabellones.length);
                    // console.log(this.pabellones);
                    if(id==2){
             
                     //this.getCrianzaCerdo();
                     this.mortalidad.getUltimacrianzaCerdo().subscribe((data) => { // Success
                          if(Object.keys(data).length>0){
                              this.id_crianza=data[0]["id_crianza"];
                             this.origen=data[0]["origen"];
                               // console.log(data);
                              loading.dismiss();
                          }else{
                            this.showAlert("Error","No se ecnontro crianza de cerdos");
                            this.navCtrl.setRoot(DireccionPage);
                          }
                         
                      }
                   );
                 }else if(id==1){
              
                     //this.getCrianzaAves();
                     this.mortalidad.getUltimacrianzaAves().subscribe((data) => { // Success
                      if(Object.keys(data).length>0){
                        this.id_crianza=data[0]["id_crianza"];
                       this.origen=data[0]["origen"];
                         // console.log(data);
                        loading.dismiss();
                    }else{
                      this.showAlert("Error","No se ecnontro crianza de aves");
                      this.navCtrl.setRoot(DireccionPage);
                    }
                      }
                    );
               }
              }else{
                this.showAlert("Error","No existe sector");
                this.navCtrl.setRoot(DireccionPage);
              }

            }
          );
          }else{
            this.showAlert("Error","No existe sector");
           this.navCtrl.setRoot(DireccionPage);
          }
        }
      );

        //this.getSectores(id);
        //this.getPabellon(id);
    
    
       
        
      }
    

    obtenerHora(){
        let fecha = new Date(Date.now());
        let hora = new Date(Date.now());
        let month = (fecha.getMonth()+1)+'';
        let minutos = (hora.getMinutes()+'');
     
        month = month.length == 1 ? '0'+month : month;
     
        let day = fecha.getDate() + '';
        day =  day.length == 1 ? '0'+day : day;
        minutos = minutos.length == 1 ? '0'+minutos : minutos;
     
        this.fecha.fechaInicio = fecha.getFullYear() + '-' + month + '-' + day;
        //this.fecha.fechaInicioCrianza = fecha.getFullYear() + '-' + month + '-' + day;
        this.fecha.horaInicio = hora.getHours() + ':' + minutos;


    }

    getSectores(idSector){
        var id ={"id":idSector};
        //console.log(id);
        this.alimento.GetSectores(id).subscribe((data) => { // Success
          if(Object.keys(data).length>0){
             this.sectorGuia={"sector":data[0]["nombre_corto"],"id":idSector};
          }else{
            this.showAlert("Error","No existen sectores");
            this.navCtrl.setRoot(DireccionPage);
          }
         
        }
      );
    }

    getPabellon(idSector){
        var id_sector={"id_sector":idSector};
        this.alimento.GetPabellones(id_sector).subscribe((data) => { // Success
          if(Object.keys(data).length>0){
              this.pabellones=data;
              this.cantidadP=(this.pabellones.length);
            // console.log(this.pabellones);
          }else{
            this.showAlert("Error","No existen pabellones");
            this.navCtrl.setRoot(DireccionPage);
          }
          
        }
      );
    }

    getCrianzaCerdo(){
        this.mortalidad.getUltimacrianzaCerdo().subscribe((data) => { // Success
          if(Object.keys(data).length>0){
            this.id_crianza=data[0]["id_crianza"];
            this.origen=data[0]["origen"];
           // console.log(data);
          }else{
            this.showAlert("Error","no se encontro crianza de cerdo");
            this.navCtrl.setRoot(DireccionPage);
          }
           
        }
      );
    }

    getCrianzaAves(){
        this.mortalidad.getUltimacrianzaAves().subscribe((data) => { // Success
          if(Object.keys(data).length>0){
            this.id_crianza=data[0]["id_crianza"];
            this.origen=data[0]["origen"];
           // console.log(data);
          }else{
            this.showAlert("Error","no se encontro crianza de aves");
            this.navCtrl.setRoot(DireccionPage);
          }
        }
      );
    }

    nuevoPabellon() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Seleccione el numero de pabellón');

        this.pabellones.forEach(element => {
            alert.addInput({
                type: 'radio',
                label: element.nro_pabellon,
                value: element.nro_pabellon,
                checked: false
              });
        });

        alert.addButton('Cancelar');

        alert.addButton({
          text: 'Ingresar',
          handler: data => {
              if (data){
                var datos={"sector":this.sectorGuia,"pabellon":data,"nro_crianza":this.id_crianza};
                //console.log(datos);
                this.navCtrl.push('PabellonPage',{
                        datos:datos
                });
              }
            console.log(data);
          }
        });
        alert.present();
        
    }

  
    showAlert(titulo,mensaje) {
      const alert = this.alertCtrl.create({
        title: titulo,
        subTitle: mensaje,
        buttons: ['OK']
      });
      alert.present();
    }
}
