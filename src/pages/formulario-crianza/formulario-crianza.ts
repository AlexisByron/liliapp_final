import { MemoryProvider } from './../../providers/memory/memory';
import { GeneralProvider } from './../../providers/general/general';
import { CommunicationsProvider } from './../../providers/communications/communications';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import {EntregaAlimentoProvider} from '../../providers/entrega-alimento/entrega-alimento';
import {CrianzaProvider} from '../../providers/crianza/crianza'; 
import { AlertController } from 'ionic-angular';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { LoadingController } from 'ionic-angular';
import {DireccionPage} from '../direccion/direccion';
import 'rxjs/add/operator/timeout';
/**
 * Generated class for the FormularioCrianzaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-formulario-crianza',
  templateUrl: 'formulario-crianza.html',
})
export class FormularioCrianzaPage {

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, public coms: CommunicationsProvider, public general: GeneralProvider, private toastCtrl: ToastController, public memory: MemoryProvider,public alimento :EntregaAlimentoProvider,public crianza:CrianzaProvider,public alertCtrl: AlertController) {
  }


  public id_sector;
  public pabellones;
  public origenes;
  public permiso;
  //informacion
  
  
    public idCrianza: null;
    public sector: null;
    public idPabellon: null;
   
    public Usuario: null;
    public animal: null;
    public fechaInicio: null;
 
    public origen: null;
    public cantidadInicial: null;
    public sexo: null;
    public pesoPromedioInicial: null;
    public edadInicial: null;
    //campos de ave inicio

    public edadReproductora: null;
    public edadReproductoraNueva: null;
    public edadReproductoraMediana: null;
    public edadReproductoraVieja: null;
    //fin campos ave

    public medidorLuz: null;
    public medidorAgua: null;

  


  public fecha = {
    fechaInicio: null,
    fechaInicioCrianza: null,
    horaInicio: null
  }

  

  ionViewDidLoad(){
    
    console.log("user profile en load", this.memory.userProfile);
    this.Usuario=this.memory.userProfile["username"];
    this.general._menu.enable(true,'main-menu');
    this.general.controller = this;
    
  }

  ionViewWillEnter(){

    this.permiso=this.memory.userProfile["permisos"];
    console.log(this.permiso);
    if(this.memory.userProfile["permisos"]=== 'ave') {
      this.id_sector=1;
    } else if (this.memory.userProfile["permisos"] === 'cerdo') {
      this.id_sector=2;
  
    }
    this.presentLoadingDefault();
  
    

   
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      // spinner: 'hide',
      content: 'Cargando información'
    });
  
    loading.present();

    var id ={"id":this.id_sector};
    console.log(id);
    this.alimento.GetSectores(id).subscribe((data) => { // Success
      if(Object.keys(data).length>0){
          console.log(data);
      this.sector=(data[0]["nombre_corto"]);

      var id_sector={"id_sector":this.id_sector};
      //console.log(id_sector)
      console.log(id_sector);
      this.alimento.GetPabellones(id_sector).subscribe((data) => { // Success
        if(Object.keys(data).length>0){
                     console.log(data);
                   this.pabellones=(data);
                    var id ={"id":this.id_sector};
                   this.alimento.GetOrigenes(id).subscribe((data) => { // Success
                    if(Object.keys(data).length>0){
                      console.log(data);
                      this.origenes=data;
                      this.obtenerHora();
                      loading.dismiss();
                    }else{
                        this.showAlert("Error","No se encontraron origenes");
                       this.navCtrl.setRoot(DireccionPage);
                    }
                   
               }
             );
        }else{
          this.showAlert("Error","No se encontraron pabellones");
          this.navCtrl.setRoot(DireccionPage);
        }

       
      }
    );
      }else{
        this.showAlert("Error","No se encontro sector");
        this.navCtrl.setRoot(DireccionPage);
      }
    
    }
  );


    //this.getSectores();
    //this.Getpabellones();
    this.obtenerHora();
   // this.obtenerOrigenes();


    
  }


  Getpabellones(){
    var id_sector={"id_sector":this.id_sector};
      //console.log(id_sector)
      console.log(id_sector);
      this.alimento.GetPabellones(id_sector).subscribe((data) => { // Success
        if(Object.keys(data).length>0){
          console.log(data);
          this.pabellones=(data);
        }else{
          this.showAlert("Error","No se encontraron pabellones");
           this.navCtrl.setRoot(DireccionPage);
        }
        
      }
    );
  }

  getSectores(){
    var id ={"id":this.id_sector};
    console.log(id);
    this.alimento.GetSectores(id).subscribe((data) => { // Success
      if(Object.keys(data).length>0){
        console.log(data);
        this.sector=(data[0]["nombre_corto"]);
      }else{
        this.showAlert("Error","No se encontraron sectores");
           this.navCtrl.setRoot(DireccionPage);
      }
      
    }
  );
  }

  
  obtenerHora(){
    let fecha = new Date(Date.now());
    let hora = new Date(Date.now());
    let month = (fecha.getMonth()+1)+'';
    let minutos = (hora.getMinutes()+'');
 
    month = month.length == 1 ? '0'+month : month;
 
    let day = fecha.getDate() + '';
    day =  day.length == 1 ? '0'+day : day;
    minutos = minutos.length == 1 ? '0'+minutos : minutos;
 
    this.fecha.fechaInicio = fecha.getFullYear() + '-' + month + '-' + day;
    //this.fecha.fechaInicioCrianza = fecha.getFullYear() + '-' + month + '-' + day;
    this.fecha.horaInicio = hora.getHours() + ':' + minutos;
    this.fecha.fechaInicioCrianza=fecha.getFullYear() + '-' + month + '-' + day;;
    console.log(this.fecha.fechaInicioCrianza);
  }

  obtenerOrigenes(){
    var id ={"id":this.id_sector};
    this.alimento.GetOrigenes(id).subscribe((data) => { // Success
      if(Object.keys(data).length>0){
         console.log(data);
     this.origenes=data;
      }else{
        this.showAlert("Error","No se encontraron origenes");
           this.navCtrl.setRoot(DireccionPage);
      }
     
    }
  );
      
    
  }
 
  submit() {
    console.log("ok ahora mostrar todos los datos y luego diferenciar");
    if(this.permiso=="ave"){
      var ave ={
        
        "idCrianza": this.idCrianza,
        "sector": this.sector,
        "idPabellon": this.idPabellon,
        "Usuario": this.Usuario,
        "peso":this.pesoPromedioInicial,
        "edadReproductora":this.edadReproductora,
        "fechaInicio": this.fecha.fechaInicioCrianza,
        "origen": this.origen,
        "cantidadInicial": this.cantidadInicial,
        "sexo": this.sexo,
        "edadInicial": this.edadInicial, 
        "medidorLuz": this.medidorLuz,
        "medidorAgua": this.medidorAgua,
        "sector_id":this.id_sector,
        "fecha_registro":this.fecha.fechaInicio+" "+this.fecha.horaInicio,
       //campos de ave inicio
        
        "edadReproductoraNueva": this.edadReproductoraNueva,
        "edadReproductoraMediana": this.edadReproductoraMediana,
        "edadReproductoraVieja": this.edadReproductoraVieja,
       //fin campos ave
       
       }
       this.crianza.AgregarCrianzaAve(ave).timeout(5000).subscribe((data) => { // Success
        this.showAlert("Exito","Crianza de ave agregada");
        console.log(data);
      },()=>{
        this.showAlert("Error","No se logro agrear crianza de ave")
      }
    );
    }else if(this.permiso=="cerdo"){
      var cerdo ={
        
        "idCrianza": this.idCrianza,
        "sector": this.sector,
        "idPabellon": this.idPabellon,
        "Usuario": this.Usuario,
        "peso":this.pesoPromedioInicial,
        "edadReproductora":this.edadReproductora,
        "fechaInicio": this.fecha.fechaInicioCrianza,
        "origen": this.origen,
        "cantidadInicial": this.cantidadInicial,
        "sexo": this.sexo,
        "edadInicial": this.edadInicial, 
        "medidorLuz": this.medidorLuz,
        "medidorAgua": this.medidorAgua,
        "sector_id":this.id_sector,
        "fecha_registro":this.fecha.fechaInicio+" "+this.fecha.horaInicio
       }

       this.crianza.AgregarCrianzaCerdo(cerdo).timeout(5000).subscribe((data) => { // Success
        this.showAlert("Exito","Crianza de cerdo agregada");
        console.log(data);
      },()=>{
        this.showAlert("Error","No se logro agregar crianza de cerdo");
      }
    );
       
    }
    
    cerdo=null;
    ave=null;

   
    this.limpar();
    this.obtenerHora();
  }

  limpar(){
    this.idCrianza= null;
    //this.sector= null;
    this.idPabellon= null;
   
    //this.Usuario= null;
    this.animal= null;
    //this.fechaInicio= null;
 
    this.origen= null;
    this.cantidadInicial= null;
    this.sexo= null;
    this.pesoPromedioInicial= null;
    this.edadInicial= null;
    //campos de ave inicio

    this.edadReproductora= null;
    this.edadReproductoraNueva= null;
    this.edadReproductoraMediana= null;
    this.edadReproductoraVieja= null;
    //fin campos ave

    this.medidorLuz= null;
    this.medidorAgua= null;
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Su formulario se ha enviado con éxito',
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  presentError(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  showAlert(titulo,mensaje) {
    const alert = this.alertCtrl.create({
      title: titulo,
      subTitle: mensaje,
      buttons: ['OK']
    });
    alert.present();
  }

}
