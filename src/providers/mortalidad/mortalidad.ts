import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the MortalidadProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MortalidadProvider {

  constructor(public http: HttpClient) {
    console.log('Hello MortalidadProvider Provider');
  }

  getMortalidades(id){
    return this.http.post("https://agrosuper.tecnoandina.cl/getmortalidades",id);
  }

  getMortalidadesAsistidas(id){
    return this.http.post("https://agrosuper.tecnoandina.cl/getmortalidadesasistidas",id);
  }

  getUltimacrianzaCerdo(){
    return this.http.get("https://agrosuper.tecnoandina.cl/getallcrianzascerdo");
  }

  getUltimacrianzaAves(){
    return this.http.get("https://agrosuper.tecnoandina.cl/getallcrianzaaves");
  }

  InsertMortalidadCerdos(datos){
    return this.http.post("https://agrosuper.tecnoandina.cl/insertmortalidadcerdos",datos);
  }
  
  InsertMortalidadAves(datos){
    return this.http.post("https://agrosuper.tecnoandina.cl/insertmortalidadaves",datos);
  }

  //



}
