import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CrianzaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CrianzaProvider {

  constructor(public http: HttpClient) {
    console.log('Hello CrianzaProvider Provider');
  }

  AgregarCrianzaCerdo(datos){
    return this.http.post("https://agrosuper.tecnoandina.cl/addcrianzacerdo",datos);
  }

  AgregarCrianzaAve(datos){
    return this.http.post("https://agrosuper.tecnoandina.cl/addcrianzaave",datos);
  }

}
