import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the EntregaAlimentoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EntregaAlimentoProvider {

  constructor(public http: HttpClient) {
    console.log('Hello EntregaAlimentoProvider Provider');
  }

  GetDietas(permiso){
    return this.http.post("https://agrosuper.tecnoandina.cl/getdietas",permiso);
  }

  GetSectores(id){
    return this.http.post("https://agrosuper.tecnoandina.cl/getsectores",id);
  }

  
  GetPabellones(id){
    return this.http.post("https://agrosuper.tecnoandina.cl/getpabellon",id);
  }

  GetIdSilo(id_pabellon){
    return this.http.post("https://agrosuper.tecnoandina.cl/getidsilo",id_pabellon);
  }

  GetGuias(id_guia){
    return this.http.post("https://agrosuper.tecnoandina.cl/getguia",id_guia);
  }


  AgregarDescargaAlimento(datos){
    return this.http.post("https://agrosuper.tecnoandina.cl/agregaringresoalimento",datos);
  }

  AgregarGuiaDespacho(datos){
    return this.http.post("https://agrosuper.tecnoandina.cl/insertguia",datos);
  }


  GetOrigenes(dato){
    return this.http.post("https://agrosuper.tecnoandina.cl/getorigenes",dato);
  }


}
